package com.easy.deploy.util;

import com.easy.enums.EasyJenkinsEnum;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tanyongpeng
 * <p></p>
 **/
public class LogUtil {

    private static final List<String> deployList = new ArrayList<>(64);

    private static final Map<String,String> deployMap = new HashMap<>(16);

    private static final Logger logger = Logger.getLogger(EasyJenkinsEnum.EASY_JENKINS.getParam());

    public static void severe(String msg){
        logger.log(Level.SEVERE,msg);
    }

    public static void warning(String msg){
        logger.log(Level.WARNING,msg);
    }

    public static void info(String msg){
        logger.log(Level.INFO,msg);
    }

    public static void info(String msg,String connectId,String host){
        logger.log(Level.INFO,msg);
        deployList.add(connectId+"="+msg);
        deployMap.put(host+"="+connectId,msg);
        try {
            Thread.sleep(1200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (msg.equals(EasyJenkinsEnum.SUCCESSFULLY_DEPLOYED.getParam())){
            clearMsg(connectId);
        }
        if (deployMap.get(host+"="+connectId) != null){
            if (deployMap.get(host+"="+connectId).equals(EasyJenkinsEnum.SUCCESSFULLY_DEPLOYED.getParam())){
                deployMap.remove(host+"="+connectId);
            }
        }
    }

    public static List<String> getDeployList() {
        return deployList;
    }

    public static Map<String, String> getDeployMap() {
        return deployMap;
    }

    public static List<String> getMsgList(String connectId){
        List<String> list = new ArrayList<>();
        for (String s : deployList) {
            if (s.contains(connectId)) {
                list.add(s);
            }
        }
        return list;
    }

    public static void clearMsg(String connectId){
        deployList.removeIf(s -> s.contains(connectId));
    }

    public static boolean getDeployState(String connectId){
        for (String deploy : deployList) {
            if (deploy.contains(connectId)){
                return true;
            }
        }
        return false;
    }

}
