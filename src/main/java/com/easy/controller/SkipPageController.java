package com.easy.controller;

import com.easy.bean.DataBranch;
import com.easy.bean.DataStructure;
import com.easy.bean.DeployRecord;
import com.easy.deploy.util.LogUtil;
import com.easy.deploy.vo.DeployConnect;
import com.easy.service.DataStructureService;
import com.easy.service.QueryDataStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author tanyongpeng
 * <p>页面跳转控制器</p>
 **/
@Controller
public class SkipPageController {

    @Autowired
    QueryDataStructureService queryDataStructureService;

    @GetMapping(value = {"/","","/index"})
    public String toMain(Model model, HttpServletRequest request){
        DataStructure dataStructure = queryDataStructureService.getDataStructure();
        List<DeployConnect> deployConnectList = dataStructure.getDeployPathMap().get(dataStructure.getActive());
        for (DeployConnect deployConnect : deployConnectList) {
            deployConnect.setDeployState(LogUtil.getDeployState(deployConnect.getConnectId()));
        }
        model.addAttribute("deployConnectList",deployConnectList);
        model.addAttribute("branch",dataStructure.getActive());
        String ws = "ws://"+request.getServerName()+":"+dataStructure.getEasyJenkinsPort()+"/socket/deploy";
        model.addAttribute("wsUrl",ws);
        return "index";
    }

    @GetMapping("/branch")
    public String toBranch(Model model, HttpServletRequest request){
        DataStructure dataStructure = queryDataStructureService.getDataStructure();
        List<DataBranch> branchList = DataStructureService.initFile(dataStructure.getDataPath(),"branch","branch",DataBranch.class);
        model.addAttribute("branchList",branchList);
        model.addAttribute("branch",dataStructure.getActive());
        return "branch";
    }

    @GetMapping("/record")
    public String toRecord(Model model, HttpServletRequest request){
        DataStructure dataStructure = queryDataStructureService.getDataStructure();
        List<DeployRecord> deployRecordList = DataStructureService.initFile(dataStructure.getDataPath(),"record",dataStructure.getActive(),DeployRecord.class);
        deployRecordList.sort((m1, m2) -> m2.getCreateTime().compareTo(m1.getCreateTime()));
        model.addAttribute("deployRecordList",deployRecordList);
        model.addAttribute("deployCount",deployRecordList.size());
        Map<Object, Integer> map = new TreeMap<>();
        for (DeployRecord record : deployRecordList) {
            if (map.get(record.getTypeName()) == null){
                map.put(record.getTypeName(),0);
            }else {
                map.put(record.getTypeName(),map.get(record.getTypeName())+1);
            }
        }
        model.addAttribute("vueCount",map.get("vue")==null?0:map.get("vue"));
        model.addAttribute("springbootCount",map.get("springboot")==null?0:map.get("springboot"));
        return "record";
    }

    @GetMapping("/settings")
    public String toSettings(Model model, HttpServletRequest request){
        DataStructure dataStructure = queryDataStructureService.getDataStructure();
        model.addAttribute("dataStructure",dataStructure);
        return "settings";
    }
}
